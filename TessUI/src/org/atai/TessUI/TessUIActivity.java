/*
 *
 *  Copyright 2011, 2013, 2014, 2017, 2018 Li-Cheng (Andy) Tai
 *                 atai@atai.org
 *                 All Rights Reserved.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */

package org.atai.TessUI;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat ;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;

import org.atai.TessUI.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.googlecode.tesseract.android.ResultIterator;
import com.googlecode.tesseract.android.TessBaseAPI;

import org.kamranzafar.jtar.TarInputStream;
import org.kamranzafar.jtar.TarEntry;


/**
 * @author atai
 *
 */
public class TessUIActivity extends Activity {
    public static final String TAG = "org.atai.TessUI.TessUIActivity";
    private static final boolean DEBUG = true;
  
    protected Button select_button, recognize_button, capture_button;
    protected EditText result_display;
    protected ImageWithDrawingView image_display;
    protected Bitmap image_display_bitmap, recognition_bitmap;
    protected Spinner language_selector, translation_target_language_selector;
    protected String lang, train_data_path, train_data_path_tessdata;
    protected TessUIActivity the_act;
    protected Intent camera_intent, select_intent;
    protected ProgressDialog progress_dialog = null;
    protected ImageButton rotate_c_button, rotate_cc_button;

    protected VScrollView mScrollView;
    protected CheckBox mShowRecognitionCheckBox;
    protected Uri image_uri;
    protected String image_path;
    
    protected static final int CAMERA_REQUEST = 0, SELECT_REQUEST = 1;
    protected static final int MAX_WIDTH = 1024 * 3 / 2, MAX_HEIGHT = 1024 * 3 / 2;

    protected String [] asset_files;
    protected boolean[] asset_copy_flags;

    protected Resources res;
    
    protected SharedPreferences preferences = null;
    protected ArrayAdapter<CharSequence> adapter = null;
    protected ArrayAdapter<CharSequence> translation_adapter = null;
    
    // preference keys 
    static final String LANGUAGE = "language";
    static final String SHOW_RESULT = "show_result";

    // keep the below in sybc with R.array.language_array
    private static final String LANG_SUFFIXES[] = {
        "eng", 
        "chi_tra", 
        "chi_sim", 
        "ara",
        "ces",
        "dan",
        "deu",
        "ell", 
        "enm",
        "epo",
        "equ",
        "est",
        "fin",
        "fra",
        "grc",
        "heb",
        "hin",
        "hrv",
        "hun",
        "ind",
        "isl",
        "ita",
        "jpn",
        "kor",
        "lav",
        "lit",
        "mal",
        "mkd",
        "mlt",
        "msa",
        "nld",
        "nor",
        "pol",
        "por",
        "ron",
        "rus",
        "slk",
        "slv",
        "spa",
        "sqi",
        "srp",
        "swa",
        "swe",
        "tam",
        "tel",
        "tgl",
        "tha",
        "tur",
        "ukr",
        "vie"        
    };

    // start of  internal Messages
    private static final int PROGRESS_DIALOG_PROGRESS = 1;

    // end of internal messages    
    
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) 
        {
            switch (msg.what)
            {
            case PROGRESS_DIALOG_PROGRESS:
                int progress = msg.arg1;
                if (progress_dialog != null)
                    progress_dialog.setProgress(progress);
                break;
            default:
                break;
                
            }
            
        }
        
    };
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        the_act = this;
        res = getResources();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        train_data_path = Environment.getExternalStorageDirectory() + "/tesseract/";
        train_data_path_tessdata = train_data_path + "tessdata/";
        image_uri = null;
        image_display = (ImageWithDrawingView) findViewById(R.id.image_display);
        image_display.setScaleType(ImageView.ScaleType.MATRIX);
        image_display_bitmap = ((BitmapDrawable)image_display.getDrawable()).getBitmap();
        Log.v(TAG, "image view bound  " + image_display_bitmap.getWidth() + ", " + image_display_bitmap.getHeight());
        
        result_display = (EditText) findViewById(R.id.result_display);
        select_button = (Button) findViewById(R.id.select_button);
        select_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(Intent.ACTION_PICK );

                i.setType("image/*");
                Intent c = Intent.createChooser(i, res.getString(R.string.select_image_file));
                select_intent = c;
                startActivityForResult(c, SELECT_REQUEST);

            }
        });
        camera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        capture_button = (Button) findViewById(R.id.capture_button);
        capture_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View V)
            {
                if (camera_intent.resolveActivity(getPackageManager()) != null) {
                    
                    // copied from Android documentation 
                    // https://developer.android.com/training/camera/photobasics
                    
                    // Create an image file name
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName = "PNG_" + timeStamp + "_";
                    File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                    File image = null;
                    image_path = null;
                    try { 
                        image = File.createTempFile(
                            imageFileName,  /* prefix */
                            ".png",         /* suffix */
                            storageDir      /* directory */
                        );
                        image_path = image.getAbsolutePath();
                        Log.v(TAG, "capture image file path is " + image_path);

                    }
                    catch (IOException ex) {
                        Log.e(TAG, "IOError: " + ex);    
                    }
                    if (image != null) {
                        Uri photoURI = FileProvider.getUriForFile(the_act,
                                                  "org.atai.TessUI.fileprovider",
                                                  image);
                        camera_intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        camera_intent.setClipData(ClipData.newRawUri("", photoURI));
                        camera_intent.addFlags(
                            Intent.FLAG_GRANT_READ_URI_PERMISSION | 
                            Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        // from https://stackoverflow.com/a/47050327
 
                        startActivityForResult(camera_intent, CAMERA_REQUEST);
                    }
                    else {
                        Log.e(TAG, "Failed to create file for image capture");
                    }
                }
                else {
                   Log.e(TAG, "No app to capture image withn the camera");    
                }
            }
        });
        recognize_button = (Button) findViewById(R.id.recognize_button);
        recognize_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (language_data_exists(lang) == false)
                {
                    new AlertDialog.Builder(the_act).setTitle(res.getString(R.string.error)).setMessage(res.getString(R.string.language_not_exists, lang)).setNeutralButton(R.string.close, null).show();
                    return;
                }
                if (image_display_bitmap == null)
                {
                    new AlertDialog.Builder(the_act).setTitle(res.getString(R.string.error)).setMessage(res.getString(R.string.no_image_no_recognition)).setNeutralButton(R.string.close, null).show();
                    return;
                }
                if (progress_dialog != null) 
                {
                    Log.e(TAG, "cannot start recognizing while another operation in progress...");
                    return;
                }
                
                try {
                    enable_buttons(false);
                    Bitmap image = image_display_bitmap;
                    recognition_bitmap = null;
                    if (image.getConfig() == Bitmap.Config.ARGB_8888)
                        recognition_bitmap = image;
                    else
                        recognition_bitmap = image.copy(Bitmap.Config.ARGB_8888, false);

                    new recognize_task().execute(recognition_bitmap);
                    progress_dialog = ProgressDialog.show(the_act, "", res.getString(R.string.recognizing), true);
                } catch ( java.lang.OutOfMemoryError ex) {
                    new AlertDialog.Builder(the_act).setTitle(res.getString(R.string.error)).setMessage(R.string.image_too_big).setNeutralButton(R.string.close, null).show();
                    enable_buttons(true);
                }
            }
        });
        language_selector = (Spinner) findViewById(R.id.language_selector);
        adapter = ArrayAdapter.createFromResource(
                this, R.array.language_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        language_selector.setAdapter(adapter);
        language_selector.setOnItemSelectedListener(new lang_select_listener());
        lang = "eng";
        preferences = getPreferences(MODE_PRIVATE);
        lang = preferences.getString(LANGUAGE, lang);
                        
        result_display.setText("");

        registerForContextMenu(result_display);


        rotate_c_button = (ImageButton) findViewById(R.id.rotatec_button);
        rotate_cc_button = (ImageButton) findViewById(R.id.rotatecc_button);

        rotate_c_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                rotate_image(90);
            }
        });

        rotate_cc_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                rotate_image(-90);
            }
        });

        mShowRecognitionCheckBox = (CheckBox) findViewById(R.id.show_recognition_result_check_box);
        mScrollView = (VScrollView) findViewById(R.id.top_scroll_view);
        
        boolean show_overlay = preferences.getBoolean(SHOW_RESULT, true);
        image_display.setDrawOverlay(show_overlay);
        mShowRecognitionCheckBox.setChecked(show_overlay);
        
        mScrollView.addToTouchHandlingViews(image_display);
        {
            File sd_dir = new File(train_data_path_tessdata );
            if (sd_dir.exists() == false)
            {
                sd_dir.mkdirs();
            }
            CopyAssetsTrigger();
        }

    }

    public void onShowRecognitionCheckboxClicked(View view) {
        boolean show_overlay = mShowRecognitionCheckBox.isChecked();
        image_display.setDrawOverlay(show_overlay);
        preferences.edit().putBoolean(SHOW_RESULT, show_overlay).commit();
    }
    
    private boolean language_data_exists(String lang) {
        File t = new File(train_data_path_tessdata + lang + ".traineddata");
        boolean r = t.exists();
        if (DEBUG) Log.v(TAG, "training data for " + lang + " exists? " + r);
        return r;
        
    }
    
    private void set_language(String language) {
        lang = language;
        int pos = -1;
        int i = 0;
        for (String item: LANG_SUFFIXES) {
            if (item.equals(lang)) {
                pos = i; 
                break;
            }
            i++;
        }
        if (pos >= 0)
        {
            if (pos != language_selector.getSelectedItemPosition())
                language_selector.setSelection(pos);
            else 
                ensure_language_data_present(pos);
        }
    }
    
    private Bitmap boundImageSize(Bitmap old) {
        Bitmap r = old;
        if (old != null) {
            int w = old.getWidth();
            int h = old.getHeight();
            if (w > MAX_WIDTH || h > MAX_HEIGHT) /* have to scale down */
            {
                int wratio = (int)((((double) w)/ MAX_WIDTH ) + 0.5);
                int hratio = (int)((((double) h)/ MAX_WIDTH ) + 0.5);
                int ratio = hratio > wratio ? hratio : wratio;
                w /= ratio;
                h /= ratio;
                r = Bitmap.createScaledBitmap(old, w, h, false);
                Log.v(TAG, "image view bound  " + r.getWidth() + ", " + r.getHeight());
                if (r != old) {
                    old.recycle();
                    old = null;
                }
                System.gc();
            }
        }
        return r;
    }
    
    private void rotate_image(int degree)
    {
        System.gc();
        if (image_display_bitmap != null)
        {
            Bitmap old ;
            image_display_bitmap = boundImageSize(image_display_bitmap);
            int w = image_display_bitmap.getWidth();
            int h = image_display_bitmap.getHeight();
            
            Matrix m = new Matrix();
            m.postRotate(degree);
            old = image_display_bitmap;
            try
            {
                
                image_display_bitmap = Bitmap.createBitmap(old, 0, 0, w, h, m, false);
                Log.v(TAG, "image view bound  " + image_display_bitmap.getWidth() + ", " + image_display_bitmap.getHeight());
                old.recycle();
                old = null;
                
                setImageDisplay(image_display_bitmap);
            }
            catch (Error e)
            {
                AlertDialog a = new AlertDialog.Builder(the_act).create();
                a.setTitle(res.getString(R.string.error));
                a.setMessage(res.getString(R.string.image_too_big_cannot_rotate));
                a.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                a.show();
                image_display_bitmap = old;
                Log.v(TAG, "image view bound  " + image_display_bitmap.getWidth() + ", " + image_display_bitmap.getHeight());
            }
        }


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (RESULT_OK == resultCode)
        {
            result_display.setText("");
            switch(requestCode)
            {
            case CAMERA_REQUEST: /* camera intent */

                File f = new File(image_path);
                image_uri = Uri.fromFile(f);
                Log.v(TAG, "Camera returns image uri " + image_uri);

                setImageDisplay(null);
                break;
            case SELECT_REQUEST:
                /* intent was to select a static image */
                image_uri = data.getData();
                Log.v(TAG, "file select returns image uri " + image_uri);
                setImageDisplay(null);
                break;
            }
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
        case R.id.about_menu:
            Intent about_intent = new Intent(TessUIActivity.this, AboutActivity.class);

            startActivity(about_intent);
            break;
        }
        return true;
    }

    public class RecognizedText {
        public String text;
        Rect rect;
        
    }
    
    public class RecognitionResults {
        public String  all_text;
        ArrayList<RecognizedText> items;
        public RecognitionResults() {
            items = new ArrayList<RecognizedText>();
        }
        public void add(RecognizedText item) {
            items.add(item);
        }
        
        RecognizedText get(int index) {
            return items.get(index);
        }
        
        int size() {
            return items.size();
        }
        
    };
    
    private class recognize_task extends AsyncTask<Bitmap, Void, RecognitionResults>
    {
        protected RecognitionResults doInBackground(Bitmap... image)
        {
            File external_dir = new File(train_data_path);
            RecognitionResults results;
            try
            {
                results = new RecognitionResults();
                TessBaseAPI base_api = new TessBaseAPI();
                base_api.init(external_dir.toString(), lang);

                base_api.setPageSegMode(TessBaseAPI.PageSegMode.PSM_AUTO_OSD);
                base_api.setImage(image[0]);
                String recognized_text = base_api.getUTF8Text();
                
                ResultIterator it = base_api.getResultIterator();
                it.begin();                
                
                com.googlecode.leptonica.android.Pixa pixa = base_api.getWords();
                ArrayList<Rect> rects = pixa.getBoxRects();
                for (int i = 0; i < rects.size(); i++) {
                    RecognizedText item = new RecognizedText();
                    item.text = it.getUTF8Text(TessBaseAPI.PageIteratorLevel.RIL_WORD);
                    Log.v(TAG, "recognized text: " + item.text);
                    item.rect = rects.get(i);
                    Log.v(TAG, "Rect: " + item.rect);
                    results.add(item);
                    it.next(TessBaseAPI.PageIteratorLevel.RIL_WORD);
                }
                pixa.recycle();
                base_api.end();
                results.all_text = recognized_text;
            } catch (Exception e) {
                Log.e(TAG, "Exception " + e);
                new AlertDialog.Builder(the_act).setTitle("Exception").setMessage(e.getMessage()).setNeutralButton(R.string.close, null).show();
                results = null;
            }
            return results;
        }
        protected void onPostExecute(RecognitionResults results)
        {
            result_display.setText(results.all_text);
            progress_dialog.dismiss();
            progress_dialog = null;
            enable_buttons(true);
            image_display.setRecognitionResults(results);
        }
    }

    private class copy_asset_task extends AsyncTask<Void, Void, Void>
    {
        protected Void doInBackground(Void... ignored)
        {
            TessUIActivity.this.do_copy_assets();
            return null;
        }

        protected void onPostExecute(Void ignored)
        {
            progress_dialog.dismiss();
            progress_dialog = null;
            set_language(preferences.getString(LANGUAGE, lang));  // asset copy may have blocked trainind data download, so try again if needed
            
        }

    }

    
    private class download_training_data_task extends AsyncTask<String, Void, Boolean>
    {
        protected Boolean doInBackground(String ... strings)
        {
            int count = strings.length;
            if (count != 1) {
               Log.e(TAG, "invalid arguments to    download_training_data_task");
               return false;
                
            }
            String lang = strings[0];
            boolean result = TessUIActivity.this.download_training_data(lang);
            if (DEBUG) Log.v(TAG, "downloading thread finishes");
            return result;
        }

        protected void onPostExecute(Boolean result)
        {
            if (DEBUG) Log.v(TAG, "done with downloading");
            progress_dialog.dismiss();
            progress_dialog = null;
        }

    }

    protected void ensure_language_data_present(int pos) /* pos is the index into LANG_SUFFIXES and R.array.language_array */
    {
        lang = LANG_SUFFIXES[pos];
        boolean exists = language_data_exists(lang);
        if (DEBUG) Log.v(TAG, "data for " + lang + " exists? " + exists);
        if (DEBUG) Log.v(TAG, "progress dialog? " + progress_dialog);
        
        if (progress_dialog != null)
        {
            Log.w(TAG, "a download operation in progress, so downloading is not started");
            return;                
        }
        if (!exists)
        {
            if (DEBUG) Log.v(TAG, "data for " + lang + " does not exist, so downloading...");
            progress_dialog = new ProgressDialog(the_act);
            progress_dialog.setMessage(res.getString(R.string.installing_language_data_please_wait, res.getStringArray(R.array.language_array)[pos]));
            progress_dialog.setProgressStyle(progress_dialog.STYLE_HORIZONTAL); 
            
            progress_dialog.show();
            new download_training_data_task().execute(lang);
        }
        
    }
    public class lang_select_listener implements OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent,
                                   View view, int pos, long id) {
            ensure_language_data_present(pos);

        }

        public void onNothingSelected(AdapterView parent) {
            // Do nothing.
        }
    }
    
    
    protected void onPause()
    {
        Drawable dr = image_display.getDrawable();
        if (dr != null)
            image_display_bitmap = ((BitmapDrawable)dr).getBitmap();
        else image_display_bitmap = null;
        image_display.setImageBitmap(null);
        System.gc();
        preferences.edit().putString(LANGUAGE, lang).commit();
                

        super.onPause();
    }

    protected void onResume()
    {
        if (DEBUG) Log.v(TAG, "onResume()");
        super.onResume();
        if (image_uri != null)
        {
            if (recognition_bitmap != null)
            {
                recognition_bitmap.recycle();
                recognition_bitmap = null;
            }
            try
            {
                if (image_display_bitmap != null) image_display_bitmap.recycle();
                Bitmap img = BitmapFactory.decodeStream(getContentResolver().openInputStream(image_uri));
                if (img != null) {
                    image_display_bitmap = boundImageSize(img);
                    setImageDisplay(image_display_bitmap);
                    Log.v(TAG, "image view bound  " + image_display_bitmap.getWidth() + ", " + image_display_bitmap.getHeight());
                }
                else {
                    Log.e(TAG, "cannot read captured or selected image?");
                    throw new Error();    
                }
            }
            catch (java.io.FileNotFoundException ex) {
                Log.e(TAG, "this should not happen " + ex);
                image_display_bitmap = null;
            }
            catch (Error e)
            {
                AlertDialog a = new AlertDialog.Builder(the_act).create();
                a.setTitle(res.getString(R.string.error));
                a.setMessage(res.getString(R.string.image_too_big_cannot_display));
                a.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                a.show();
                image_display_bitmap = null;
            }
            image_uri = null;
        }
        else
            image_display.setImageBitmap(image_display_bitmap);
        System.gc();
        if (DEBUG) Log.v(TAG, "do setlanguage again ");
        set_language(preferences.getString(LANGUAGE, lang));
    }

    private void setImageDisplay(Bitmap img) {
        image_display.setImageBitmap(img);
        image_display.setRecognitionResults(null);
    }

    
    private void CopyAssetsTrigger() {
        AssetManager assetManager = getAssets();
        asset_files = null;
        boolean something_to_copy = false;
        try {
            asset_files = assetManager.list("");
        } catch (IOException e) {
            Log.e(TAG, "Exception: " + e);
        }
        asset_copy_flags = new boolean[asset_files.length];
        int i = 0;
        for(String file_name : asset_files) {
            boolean to_copy = false;
            String dest_file_name = train_data_path_tessdata + file_name;
            File t = new File(dest_file_name);
            if (t.exists() == false) {
                if (DEBUG) Log.i(TAG, "destination file " + dest_file_name + " does not exist");
                to_copy = true;
            }
            else {
                try {
                    InputStream a = assetManager.open(file_name);
                    long a_length = a.available();
                    long d_length = t.length();
                    if (a_length != d_length)
                        to_copy = true;
                    if (DEBUG) Log.i(TAG, "asset length " + a_length + " dest file length " + d_length + " to_copy " + to_copy);
                    /* TO DO: use content checksum like md5 to compare files */
                } catch (IOException ex) {
                    Log.w(TAG, "Exception: " + ex);
                    to_copy = true;

                }
            }
            if (to_copy)
                something_to_copy = true;
            asset_copy_flags[i] = to_copy;
            i++;
        }
        if (something_to_copy)
        {
            new copy_asset_task().execute();
            progress_dialog = ProgressDialog.show(the_act, "", res.getString(R.string.installing_please_wait), true);
        }
    }

    private void do_copy_assets()
    {
        AssetManager assetManager = getAssets();
        int i = 0;
        for (String file_name : asset_files) {
            if (asset_copy_flags[i])
            {
                InputStream in = null;
                OutputStream out = null;
                String dest_file_name = train_data_path_tessdata + file_name;
                try {
                    if (DEBUG) Log.i(TAG, "copy " + file_name + " to " + dest_file_name);
                    in = assetManager.open(file_name);
                    out = new FileOutputStream(dest_file_name);
                    copyFile(in, out);
                    in.close();
                    in = null;
                    out.flush();
                    out.close();
                    out = null;
                } catch(Exception e) {
                    Log.e(TAG, "Exception: " + e);
                }
            }
            i++;
        }
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    private boolean download_training_data(String lang) {
        boolean result = true;
        String url_string = res.getString(R.string.training_data_tgz_url_template, lang);
        String location;
        String dest_file_name = train_data_path_tessdata + res.getString(R.string.training_data_tgz_file_name_template, lang);
        URL url, base, next;
        HttpURLConnection conn;
            
        try {
            
            while (true) {  // from http://stackoverflow.com/a/26046079/3798801
                if (DEBUG) Log.v(TAG, "downloading " + url_string);
                try {
                    url = new URL(url_string);
                } catch (java.net.MalformedURLException ex) {
                    Log.e(TAG, "url " + url_string + " is bad: " + ex);
                    return false;
                }
                conn = (HttpURLConnection) url.openConnection();
                conn.setInstanceFollowRedirects(false);
                switch (conn.getResponseCode())
                {
                case HttpURLConnection.HTTP_MOVED_PERM:
                case HttpURLConnection.HTTP_MOVED_TEMP:
                    location = conn.getHeaderField("Location");
                    base     = new URL(url_string);               
                    next     = new URL(base, location);  // Deal with relative URLs
                    url_string = next.toExternalForm();
                   continue;
                }
                
                break;            
            }
            
            conn.connect();

            int total_length = conn.getContentLength();
            // download the file
            InputStream input = new BufferedInputStream(url.openStream());
            OutputStream output = new FileOutputStream(dest_file_name);

            byte data[] = new byte[1024 * 6];
            int total_downloaded = 0;
            int count;
            if (progress_dialog != null) progress_dialog.setMax(total_length);
            while ((count = input.read(data)) != -1) {
                total_downloaded += count;
                output.write(data, 0, count);
                int progress = (int)(total_downloaded);
                Message msg = mHandler.obtainMessage(PROGRESS_DIALOG_PROGRESS);
                msg.arg1 = progress;
                mHandler.sendMessage(msg);
            }
            
            output.flush();
            output.close();
            input.close();
        } catch (IOException e) {
            result = false;
            Log.e(TAG, "failed to download " + url_string + " : " + e);
        }
        if (result) {
            if (DEBUG) Log.v(TAG, "unarchive " + dest_file_name);
            try {
                untarTGzFile(dest_file_name, train_data_path_tessdata);
            } catch (IOException e) {
                result = false;
                Log.e(TAG, "failed to ungzip/untar " + dest_file_name + " : " + e);
            }
        }
            
        return result;        
    }
    
    protected void enable_buttons(boolean flag)
    {
        select_button.setEnabled(flag);
        recognize_button.setEnabled(flag);
        capture_button.setEnabled(flag);
        language_selector.setEnabled(flag);
    }

/* code below taking from Kamran Zafar's org.kamranzafar.jtar JarTest.java code
 * Copyright 2012 Kamran Zafar
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
*/
	static final int BUFFER = 1024 * 10;
    private void untar(TarInputStream tis, String destFolder) throws IOException {
        BufferedOutputStream dest = null;

        TarEntry entry;
        while ((entry = tis.getNextEntry()) != null) {
            if (DEBUG) Log.v(TAG, "Extracting: " + entry.getName());
            int count;
            byte data[] = new byte[BUFFER];
            String file_name = entry.getName();
            if (entry.isDirectory()) {
                /*new File(destFolder + "/" + entry.getName()).mkdirs();*/
                continue;
            } else {
                int di = entry.getName().lastIndexOf('/');
                if (di != -1) {
                    /*new File(destFolder + "/" + entry.getName().substring(0, di)).mkdirs();*/
                    file_name = entry.getName().substring(di + 1, entry.getName().length());
                }
            }
            if (DEBUG) Log.v(TAG, "writing to " + file_name);

            FileOutputStream fos = new FileOutputStream(destFolder + "/" + file_name /*entry.getName()*/);
            dest = new BufferedOutputStream(fos);

            while ((count = tis.read(data)) != -1) {
                dest.write(data, 0, count);
            }

            dest.flush();
            dest.close();
        }
    }

    /**
     * Untar the gzipped-tar file
     *
     * @throws IOException
     */

    public void untarTGzFile(String tar_gz_file, String dest_path) throws IOException {

        File zf = new File(tar_gz_file);

        TarInputStream tis = new TarInputStream(new BufferedInputStream(new GZIPInputStream(new FileInputStream(zf))));

        untar(tis, dest_path);

        tis.close();
    }
    
    /* end of code from Kamran Zafar's org.kamranzafar.jtar JarTest.java code  */
}